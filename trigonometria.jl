#MAC0110-MiniEP7
#André Gustavo Nakagomi Lopez - 11796465

using Test

function sin(x)
	soma=BigFloat(x)
	sinal=-1

	for i in 3:2:21
		soma+=sinal*(x^i)/factorial(BigFloat(i))
		sinal*=-1
	end

	return round(Float64(soma),sigdigits=3)
end

function cos(x)
	soma=1
	sinal=-1

	for i in 2:2:20
		soma+=sinal*(x^i)/factorial(BigFloat(i))
		sinal*=-1
	end

	return round(Float64(soma),sigdigits=3)
end

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)

		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end	
	end
	return abs(A[1])
end

function tan(x)
	x=BigFloat(x)

	if x==BigFloat(pi)
		return 1.0
	end

	soma=x+BigFloat(x^3/3)+BigFloat(2*x^5/15)+BigFloat(17*x^7/315)+BigFloat(62*x^9/2835)

	for n in 5:10
		soma+=BigFloat(2^(2*n))*BigFloat(2^(2*n)-1)*BigFloat(bernoulli(n))*BigFloat(x^(2*n-1))/factorial(BigFloat(2*n))
	end

	return round(Float64(soma),sigdigits=3)
end

function quaseigual(v1, v2)
    erro = 0.03
    igual = abs(v1 - v2)

    if igual <= erro
        return true
    else
        return false
    end
end

function check_sin(value,x)
	if value==sin(x)
		return true
	else
		return false
	end
end

function check_cos(value,x)
	if value==cos(x)
		return true
	else 
		return false
	end
end

function check_tan(value,x)
	if quaseigual(value,tan(x))
		return true
	else
		return false
	end
end


function taylor_sin(x)
	soma=BigFloat(x)
	sinal=-1

	for i in 3:2:21
		soma+=sinal*(x^i)/factorial(BigFloat(i))
		sinal*=-1
	end

	return round(Float64(soma),sigdigits=3)
end

function taylor_cos(x)
	soma=1
	sinal=-1

	for i in 2:2:20
		soma+=sinal*(x^i)/factorial(BigFloat(i))
		sinal*=-1
	end

	return round(Float64(soma),sigdigits=3)
end

function taylor_tan(x)
	x=BigFloat(x)

	if x==BigFloat(pi)
		return 1.0
	end

	soma=x+BigFloat(x^3/3)+BigFloat(2*x^5/15)+BigFloat(17*x^7/315)+BigFloat(62*x^9/2835)

	for n in 5:10
		soma+=BigFloat(2^(2*n))*BigFloat(2^(2*n)-1)*BigFloat(bernoulli(n))*BigFloat(x^(2*n-1))/factorial(BigFloat(2*n))
	end

	return round(Float64(soma),sigdigits=3)
end


function test()
	@test check_sin(0.5,pi/6)
	@test check_sin(0.707,pi/4)
	@test check_sin(0.866,pi/3)
	@test check_sin(0.309,pi/10)

	@test check_cos(0.866,pi/6)
	@test check_cos(0.707,pi/4)
	@test check_cos(0.5,pi/3)
	@test check_cos(0.951,pi/10)

	@test check_tan(0.577,pi/6)
	@test check_tan(1,pi/4)
	@test check_tan(1.732,pi/3)
	@test check_tan(0.324,pi/10)

	println("Fim dos testes.")
end
